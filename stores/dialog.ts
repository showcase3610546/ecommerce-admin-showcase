import { defineStore } from 'pinia'
import { dialogTypeEnum } from '~/enums/dialog_type'


type State = {
    dialog_type: dialogTypeEnum,
    is_open: boolean
    message: string
}

export const useDialogStore = defineStore('dialog', {
    state: (): State => ({
        'dialog_type': dialogTypeEnum.SUCCESS,
        'is_open': false,
        'message': ''
    }),

    actions:{
        openDialog(dialog_type:dialogTypeEnum, message: string){
            this.is_open = true;
            this.dialog_type = dialog_type;
            this.message = message;
        },

        closeDialog(){
            this.is_open = false;
        }
    }
  })